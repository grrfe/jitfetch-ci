import setuptools

setuptools.setup(
    name="jitfetch-ci",
    packages=setuptools.find_packages(),
    description="Build JitPack packages from CI pipelines",
    author="grrfe",
    author_email="grrfe@420blaze.it",
    setuptools_git_versioning={"enabled": True},
    setup_requires=["setuptools-git-versioning<2"],
    url="https://gitlab.com/grrfe/jitfetch-ci",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "requests",
        "jitfetch @ git+https://gitlab.com/grrfe/jitfetch@0.0.10#egg=jitfetch"
    ],
    python_requires=">=3.0",
    entry_points={
        "console_scripts": [
            'jitfetch-ci = jitfetchci.ci:parse_input',
        ],
    }
)
