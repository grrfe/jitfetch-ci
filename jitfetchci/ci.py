#!/usr/bin/python3
import argparse
import json
import time

import requests
from jitfetch.jitfetch import has_version, trigger_version_build, request_build_log

__TELEGRAM_SEND_MESSAGE = "https://api.telegram.org/bot{0}/sendMessage"


def send_message(telegram_bot_token: str, telegram_chat_id: str, message: str):
    return requests.post(__TELEGRAM_SEND_MESSAGE.format(telegram_bot_token),
                         data={"chat_id": telegram_chat_id, "text": message, "parse_mode": "Markdown",
                               "disable_web_page_preview": True}).status_code


def get_repo_url(repo: str) -> str:
    full_host, repo = repo.split("/")
    tld, host, user = full_host.split(".")

    return f"https://{host}.{tld}/{user}/{repo}"


def attempt_build(repo: str, repo_url: str, version: str, bot_token: str, chat_id: str):
    result = trigger_version_build(repo, version)
    if result is True:
        send_message(bot_token, chat_id, f"✅ Successfully built [{repo}]({repo_url})")
    else:
        status, message = result
        if status == "tagNotFound":
            print("No tag found, sleeping..")
            time.sleep(30)
            attempt_build(repo, repo_url, version, bot_token, chat_id)
        else:
            send_message(bot_token, chat_id, f"🚫 Failed to build [{repo}]({repo_url}): `{status}`, `{message}`")
            if message.startswith("No build artifacts found"):
                build_log = request_build_log(repo, version)
                print(build_log)
                send_message(bot_token, chat_id, build_log)


def parse_input():
    parser = argparse.ArgumentParser(description="Build JitPack packages from CI pipelines")
    parser.add_argument("repo", help="Repo in the following format: com.gitlab.grrfe/jitfetch-ci")
    parser.add_argument("version", help="Fetch the given version")
    parser.add_argument("telegram_bot_token", help="Telegram bot token")
    parser.add_argument("telegram_chat_id", help="Telegram chat id")

    print("Sleeping to allow JitPack to catch up..")
    time.sleep(30)

    args = parser.parse_args()
    bot_token = args.telegram_bot_token
    chat_id = args.telegram_chat_id

    repo_url = get_repo_url(args.repo)

    versions = has_version(args.repo)
    print(json.dumps(versions, indent=4))

    if versions is None:
        send_message(bot_token, chat_id, f"🚫 Repository [{args.repo}]({repo_url}) could not be found")
        return

    attempt_build(args.repo, repo_url, args.version, bot_token, chat_id)


if __name__ == "__main__":
    parse_input()
